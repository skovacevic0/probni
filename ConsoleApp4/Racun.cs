﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4 {
    class Racun<T> {
        public Racun(T iznos) {
            Iznos = iznos;
        }

        public DateTime DatumIzdavanja { get; set; }
        public T Iznos { get; set; }
    }
}
